FROM node:12-slim

RUN mkdir -p /home/node/app

WORKDIR /home/node/app

COPY . .

ENV NODE_ENV=PRODUCTION

RUN npm install

ENV PORT=5000

EXPOSE ${PORT}

ENTRYPOINT ["npm", "start"]


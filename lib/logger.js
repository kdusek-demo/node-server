/**
 * Created by kurtdusek on 8/7/15.
 */
var winston = require('winston');
var config = require('../config/donkey.js');
var Sentry = require('winston-sentry-raven-transport');

module.exports = {
    logger: function() {
        return winston.createLogger({
            level: 'info',
            format: winston.format.combine(
              winston.format.splat(),
              winston.format.simple()
            ),
            transports: [
                new winston.transports.Console({
                    level: 'silly'
                }),
                new Sentry({
                    level: 'warn',
                    dsn: config.sentry_dsn,
                    release: 'node-serve@'+process.env.npm_package_version
                })
            ]
        })
    }
};


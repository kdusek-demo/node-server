var express         = require('express');
var bodyParser      = require('body-parser');
var _               = require('underscore');

var app = express();
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.set('port', (process.env.PORT || 5000));
app.use(express.static('public'));
// app.get('/', function (req, res, next) {
//     res.send('OK');
//     next();
// });

app.listen(app.get('port'), function(){
	console.log('info','Web Service Starting on port ', app.get('port') );
});

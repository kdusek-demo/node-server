/**
 * Created by kurtdusek on 5/4/16.
 */
var dotenv = require('dotenv');
dotenv.config();
var mongoose = require('mongoose');
module.exports = {
    stats_database:{
    },
    sentry_dsn: process.env.SENTRY_DSN
};
